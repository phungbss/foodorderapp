import 'package:flutter/material.dart';
import 'package:hijack/Model/GlobalData.dart';

class AddNotesScreen extends StatefulWidget {
  _AddNotesScreenState createState() => new _AddNotesScreenState();
}

class _AddNotesScreenState extends State<AddNotesScreen> {
  String note = Global.ORDER.note;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = Global.ORDER.note;
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Add Notes'),
    );
  }

  Widget _buildFooter() {
    return Container(
      alignment: Alignment.center,
      height: 45.0,
      color: Theme.of(context).primaryColor,
      child: Text('SAVE NOTES', style: TextStyle(color: Colors.white, fontSize: 16.0),),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Stack(
        children: <Widget>[
          Container(
            color: Theme.of(context).backgroundColor,
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
                  margin: EdgeInsets.only(top: 30.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        top: BorderSide(color: Colors.grey, width: 0.6),
                        bottom: BorderSide(color: Colors.grey, width: 0.6),
                      )),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      TextField(
                        controller: controller,
                        maxLength: 150,
                        maxLines: 5,
                        onChanged: (text) {
                          if (text.length <= 150) {
                            Global.ORDER.note = text;
                          }
                        },
                        decoration: InputDecoration(
                          hintText: 'Additional comments',
                          border: InputBorder.none,
                        ),
                      ),
                      /*Text((150 - note.length).toString())*/
                    ],
                  ),
                ),
                Spacer(),
                //_buildFooter()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
