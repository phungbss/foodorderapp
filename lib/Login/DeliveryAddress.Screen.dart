import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Widget/GroupInputFields.dart';

class DeliveryAddressScreen extends StatefulWidget {
  @override
  _DeliveryAddressScreenState createState() => new _DeliveryAddressScreenState();
}

class _DeliveryAddressScreenState extends State<DeliveryAddressScreen> {
  TextEditingController address1Controller = TextEditingController();
  TextEditingController address2Controller = TextEditingController();
  TextEditingController postcodeController = TextEditingController();

  goBackAndSave() {
    Firestore.instance.collection('users').document(Global.USER.uid).updateData({
      'address1' : address1Controller.text,
      'address2' : address2Controller.text,
      'postcode' : postcodeController.text,
    });
    updateUser();
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    super.initState();
    address1Controller.text = Global.USER.address1;
    address2Controller.text = Global.USER.address2;
    postcodeController.text = Global.USER.postcode;
  }

  Widget _buildAppBar() {
    return AppBar(
      leading: new IconButton(
        icon: new Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back, color: Colors.white),
        onPressed: goBackAndSave,
      ),
      title: Text('Delivery Address'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: ListView(
            children: <Widget>[
              GroupInputFields(
                items: <Widget>[
                  GroupInputFieldsItem(hint: 'Flat Number / Building Name', textEditingController: address1Controller,),
                  Divider(height: 1.0,),
                  GroupInputFieldsItem(hint: 'Address Line 1', textEditingController: address2Controller,),
                  Divider(height: 1.0,),
                  GroupInputFieldsItem(hint: 'Postcode', textInputType: TextInputType.number, textEditingController: postcodeController,),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}