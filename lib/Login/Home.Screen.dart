import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/MyBasket.Screen.dart';
import 'package:hijack/Login/MyOrders.Screen.dart';
import 'package:hijack/Login/MyProfile.Screen.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/Restaurant.dart';
import 'package:hijack/Widget/Carousel.dart';
import 'package:hijack/Widget/StarRating.dart';
import 'RestaurantMenu.Screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => new _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  gotoMyProfile() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyProfileScreen()),
    );
  }

  gotoSearchScreen() {}

  gotoBasketScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyBasketScreen()),
    );
  }

  gotoMyOrders() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyOrdersScreen()),
    );
  }

  gotoRestaurantDetail(restaurant) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => RestaurantMenuScreen(restaurant: restaurant)),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      leading: IconButton(
          icon: Icon(Icons.account_circle), onPressed: gotoMyProfile),
      title: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Ho Chi Minh City',
            style: TextStyle(fontSize: 18.0),
          ),
          Text('Linh Trung, Thu Duc, HCMC', style: TextStyle(fontSize: 12.0)),
        ],
      ),
      actions: <Widget>[
        IconButton(icon: Icon(Icons.layers), onPressed: gotoMyOrders)
      ],
    );
  }

  Widget _buildBanner(double screenWidth, double screenHeight) {
    return Stack(
      children: <Widget>[
        SizedBox(
          height: screenHeight * 0.25,
          child: Carousel(
            children: [
              NetworkImage(
                'https://www.peta.org.uk/wp-content/uploads/2018/01/Dominos-Follow-Your-Heart-vegan-cheese-770x513.jpg',
              ),
              NetworkImage(
                  'https://www.nenchon.vn/files/uploads/uploads_content/2016/08/Popeyes-1.jpg'),
              NetworkImage(
                  'https://cdn.mgift.vn/upload/Images/2016/12/Banner-TexaschickenVN-1440X986.jpg')
            ]
                .map((netImage) => new Image(
                      image: netImage,
                      fit: BoxFit.cover,
                      height: screenHeight * 0.25,
                      width: screenWidth,
                    ))
                .toList(),
          ),
        ),
        /*Container(
            width: screenWidth,
            alignment: Alignment.center,
            height: 40.0,
            color: Colors.grey.withOpacity(0.65),
            child: FlatButton(
                onPressed: gotoSearchScreen,
                padding: EdgeInsets.only(left: 7.0, right: 7.0),
                child: Container(
                    height: 30.0,
                    alignment: Alignment.center,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(4.0)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.search,
                          size: 18.0,
                          color: Colors.grey,
                        ),
                        Text(
                          'Search',
                          style: TextStyle(color: Colors.grey),
                        ),
                      ],
                    )
                )
            )
        ),*/
      ],
    );
  }

  Widget _buildRestaurantItem(item, id) {
    return FlatButton(
      padding: EdgeInsets.only(top: 0.0, bottom: 0.0),
      onPressed: () => gotoRestaurantDetail(new Restaurant(
          id,
          item['name'],
          item['category'],
          item['rating'] + 0.0,
          item['time'],
          item['avatarUrl'],
          item['photoUrl'],
          item['discount'],
          item['minOrder'])),
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: Colors.grey.withOpacity(0.5), width: 0.5))),
        padding: const EdgeInsets.only(
            top: 12.0, left: 0.0, right: 8.0, bottom: 12.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(0.0),
                  child: new Container(
                    margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                    child: new Container(
                      width: 70.0,
                      height: 70.0,
                    ),
                    decoration: new BoxDecoration(
                      borderRadius: new BorderRadius.circular(10.0),
                      color: Colors.grey,
                      image: new DecorationImage(
                          image: new NetworkImage(item['avatarUrl']),
                          fit: BoxFit.cover),
                      boxShadow: [
                        new BoxShadow(
                            color: Colors.grey,
                            blurRadius: 5.0,
                            offset: new Offset(2.0, 5.0))
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          item['name'],
                          style: TextStyle(
                              fontSize: 17.0, fontWeight: FontWeight.w700),
                        ),
                      ),
                      Container(
                        width: 180.0,
                        padding: const EdgeInsets.only(bottom: 4.0),
                        child: Text(
                          item['category'],
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 13.0, color: Colors.grey),
                        ),
                      ),
                      SmoothStarRating(
                        allowHalfRating: false,
                        onRatingChanged: (v) {},
                        starCount: 5,
                        rating: item['rating'] + .0,
                        size: 13.0,
                        color: Colors.amber,
                        borderColor: Colors.amber,
                      )
                    ],
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 1.0),
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(3.0)),
              child: Text(
                '${item['time']} mins',
                style: TextStyle(fontSize: 12.0),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildFloatingButton() {
    return FloatingActionButton(
      onPressed: () {
        gotoBasketScreen();
      },
      tooltip: 'Total Price: ' + Global.ORDER.totalPrice.toString(),
      elevation: 9.0,
      backgroundColor: Theme.of(context).primaryColor,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.shopping_basket,
              size: 26.0,
            ),
            Text(
              Global.ORDER.items.length.toString(),
              style: TextStyle(fontSize: 26.0),
            )
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      floatingActionButton:
          Global.ORDER.items.length > 0 ? _buildFloatingButton() : Container(),
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            _buildBanner(screenWidth, screenHeight),
            StreamBuilder(
                stream:
                    Firestore.instance.collection('restaurants').snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Text('Loading...');
                  return Container(
                    margin: EdgeInsets.only(bottom: 0.0),
                    child: Container(
                        width: screenWidth,
                        alignment: Alignment.center,
                        height: 35.0,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.grey, width: 0.5))),
                        child: Text(
                            'List of Restaurants (${snapshot.data.documents.length})')),
                  );
                }),
            Expanded(
              child: StreamBuilder(
                  stream:
                      Firestore.instance.collection('restaurants').snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) return Text('Loading...');
                    return Container(
                      child: ListView.builder(
                        itemBuilder: (context, index) => _buildRestaurantItem(
                            snapshot.data.documents[index],
                            snapshot.data.documents[index].documentID),
                        itemCount: snapshot.data.documents.length,
                      ),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
