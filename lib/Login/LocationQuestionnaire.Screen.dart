import 'package:flutter/material.dart';
import 'package:hijack/Login/Home.Screen.dart';
import 'SignUp.Screen.dart';
import 'package:hijack/Widget/Dropdown.dart' as Dropdown;

class LocationQuestionnaireScreen extends StatefulWidget {
  @override
  _LocationQuestionnaireScreenState createState() => new _LocationQuestionnaireScreenState();
}

class _LocationQuestionnaireScreenState extends State<LocationQuestionnaireScreen> {

  var listCountries = ['Ha Noi', 'Ho Chi Minh'];

  void onPressLogin() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

  void onPressForgotPassword() {

  }

  void onPressSignUpLink() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignUpScreen()),
    );
  }

  List<Dropdown.DropdownMenuItem<String>> createListCountries() {
    List<Dropdown.DropdownMenuItem<String>> list = new List();

    listCountries.forEach((country) {
        list.add(new Dropdown.DropdownMenuItem(child: Text(country)));
    });

    return list;
  }

  Widget _buildForm() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
          color: Theme.of(context).primaryColor.withOpacity(0.15),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Dropdown.DropdownButton<String>(
                items: createListCountries(),
                isDense: true,
                onChanged: (String value) {},
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: Dropdown.DropdownButton<String>(
                items: createListCountries(),
                isDense: true,
                onChanged: (String value) {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildText() {
    return Container(
      width: 270.0,
      padding: const EdgeInsets.only(bottom: 15.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text('LET US KNOW YOUR LOCATION'),
          ),
          Text('We\'d like to find the best restaurants around you', textAlign: TextAlign.center,)
        ],
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: RaisedButton(
        onPressed: onPressLogin,
        textColor: Colors.white,
        color: Theme.of(context).primaryColor,
        child: Container(
          alignment: Alignment.center,
          height: 45.0,
          child: new Text(
            "SHOW RESTAURANTS",
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Spacer(),
            _buildText(),
            _buildForm(),
            _buildButton(),
          ],
        ),
      ),
    );
  }
}