import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/Home.Screen.dart';
import 'package:hijack/Model/User.dart';
import 'SignUp.Screen.dart';

import 'package:firebase_auth/firebase_auth.dart';

import 'package:hijack/Model/GlobalData.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _email = '';
  String _password = '';

  Firestore firestore = Firestore.instance;
  FirebaseUser _currentUser;

  void onPressLogin() async {
    try {
      _currentUser = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: _email, password: _password);

      firestore
          .collection('users')
          .document(_currentUser.uid)
          .get()
          .then((data) => Global.USER = User(
                _currentUser.uid,
                data['firstname'],
                data['lastname'],
                data['email'],
                data['phone'],
                data['card'],
                data['address1'],
                data['address2'],
                data['postcode'],
              ));

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      );
    } catch (e) {
      print(e);
    }
  }

  void onPressForgotPassword() {}

  void onPressSignUpLink() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignUpScreen()),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Log In'),
    );
  }

  Widget _buildForm() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4.0)),
          color: Theme.of(context).primaryColor.withOpacity(0.15),
        ),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: TextField(
                  onChanged: (email) => _email = email,
                  keyboardType: TextInputType
                      .emailAddress, // Use email input type for emails.
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'you@example.com',
                      labelText: 'Email Address')),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: TextField(
                  onChanged: (password) => _password = password,
                  obscureText: true, // Use secure text for passwords.
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Password',
                      labelText: 'Password')),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: RaisedButton(
        onPressed: onPressLogin,
        textColor: Colors.white,
        color: Theme.of(context).primaryColor,
        child: Container(
          alignment: Alignment.center,
          height: 45.0,
          child: new Text(
            "LOGIN",
          ),
        ),
      ),
    );
  }

  Widget _buildLinks() {
    return Column(
      children: <Widget>[
        Container(
          height: 25.0,
          child: FlatButton(
            onPressed: onPressForgotPassword,
            child: Text('Forgot Password'),
          ),
        ),
        Container(
          height: 25.0,
          margin: EdgeInsets.only(bottom: 10.0),
          child: FlatButton(
            onPressed: onPressSignUpLink,
            child: Text('Don\'t have an account? Sign Up!'),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Spacer(),
            _buildForm(),
            _buildLinks(),
            _buildButton(),
          ],
        ),
      ),
    );
  }
}
