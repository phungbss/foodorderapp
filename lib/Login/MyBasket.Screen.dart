import 'package:flutter/cupertino.dart';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:hijack/Login/AddNotes.Screen.dart';
import 'package:hijack/Login/MyDeliveryDetails.Screen.dart';
import 'package:hijack/Model/FoodOrderItem.dart';
import 'package:hijack/Model/GlobalData.dart';

const double _kPickerSheetHeight = 300.0;
const double _kPickerItemHeight = 32.0;

class MyBasketScreen extends StatefulWidget {
  _MyBasketScreenState createState() => new _MyBasketScreenState();
}

class _MyBasketScreenState extends State<MyBasketScreen> {
  var isEdit = false;

  int _selectedColorIndex = 0;

  goBack() {
    Navigator.pop(context);
  }

  gotoAddNotes() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AddNotesScreen()),
    );
  }

  gotoConfirmOrder() {
    
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyDeliveryDetailsScreen()),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('My Basket'),
    );
  }

  refreshUI() {
    setState(() {});
  }

  increase(int index, double price, int discount) {
    Global.ORDER.items[index].amount += 1;
    Global.ORDER.totalPrice = round(Global.ORDER.totalPrice + price);
    Global.ORDER.priceAfterDiscount =
        round(Global.ORDER.priceAfterDiscount + price * (1 - discount / 100));
    refreshUI();
  }

  deduct(int index, double price, int discount) {
    Global.ORDER.items[index].amount -= 1;
    if (Global.ORDER.items[index].amount == 0) {
      Global.ORDER.items.removeAt(index);
      if (Global.ORDER.items.length == 0) {
        Global.ORDER.deliveryTime = 0;
        goBack();
      } else Global.ORDER.deliveryTime = Global.ORDER.items.map((i) => i.deliveryTime).toList().reduce(max);
    }
    if (Global.ORDER.items.length == 0) {
      goBack();
    }
    Global.ORDER.totalPrice = round(Global.ORDER.totalPrice - price);
    Global.ORDER.priceAfterDiscount = round(Global.ORDER.priceAfterDiscount - price * (1 - discount / 100));
    refreshUI();
  }

  clearBasket() {
    Global.ORDER.items.clear();
    Global.ORDER.totalPrice = 0.0;
    Global.ORDER.priceAfterDiscount = 0.0;
    Global.ORDER.note = '';
    Global.ORDER.deliveryTime = 0;
    goBack();
    //refreshUI();
  }

  double round(double num) {
    return double.parse(num.toStringAsFixed(2));
  }

  Widget _buildFooter() {
    return FlatButton(
      onPressed: () {
        if (!isEdit) {
          gotoConfirmOrder();
        } else
          clearBasket();
      },
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        height: 50.0,
        color: Theme.of(context).primaryColor,
        child: Text(
          isEdit ? 'CLEAR BASKET' : 'GO TO CHECKOUT',
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
      ),
    );
  }

  Widget _buildYourOrderList() {
    return Container(
      padding: EdgeInsets.only(left: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FlatButton(
            onPressed: gotoAddNotes,
            padding: const EdgeInsets.only(top: 15.0, bottom: 10.0),
            child: Text(
              '+ Add Notes',
              style: TextStyle(color: Colors.black, fontSize: 15.0),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: 15.0, bottom: 15.0, right: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'YOUR ORDER',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold),
                ),
                InkWell(
                  onTap: () => setState(() {
                        isEdit = !isEdit;
                      }),
                  child: Text(
                    isEdit ? 'Done' : 'Edit',
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                )
              ],
            ),
          ),
          Divider(
            height: 1.0,
          ),
          Column(
              children:
                  new List.generate(Global.ORDER.items.length, (int index) {
            return _buildOrderItem(Global.ORDER.items[index], index);
          })),
          FlatButton(
            onPressed: goBack,
            padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
            child: Text(
              '+ Add More Items',
              style: TextStyle(color: Colors.black, fontSize: 15.0),
            ),
          ),
          Divider(
            height: 1.0,
          ),
          _buildSubtotal(
              'Subtotal', '\$${round(Global.ORDER.totalPrice)}', true),
          _buildSubtotal(
              'Delivery Fee', '\$${round(Global.ORDER.shippingFee)}', true),
          _buildSubtotal(
              'Discount',
              '\$${round(Global.ORDER.totalPrice - Global.ORDER.priceAfterDiscount)}',
              false)
        ],
      ),
    );
  }

  Widget _buildSubtotal(String text, String price, bool showDivider) {
    return Padding(
      padding: const EdgeInsets.only(top: 15.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                    text,
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Text(
                    price,
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                )
              ],
            ),
          ),
          showDivider
              ? Divider(
                  height: 1.0,
                )
              : Container()
        ],
      ),
    );
  }

  Widget _buildTotal(
      String text, double price, bool showDivider, double priceAfterDiscount) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
        top: BorderSide(color: Colors.grey, width: 0.5),
        bottom: BorderSide(color: Colors.grey, width: 0.5),
      )),
      padding: const EdgeInsets.only(top: 15.0, left: 15.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Text(
                    text,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: Text(
                        '\$' + price.toString(),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.normal,
                            decoration: TextDecoration.lineThrough),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 15.0),
                      child: Text(
                        '\$' + round(priceAfterDiscount).toString(),
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildOrderItem(FoodOrderItem item, int index) {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    isEdit
                        ? InkWell(
                            onTap: () {
                              if (item.amount > 0) {
                                deduct(index, item.price, item.discount);
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Icon(
                                Icons.remove_circle,
                                size: 17.0,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        '${item.amount}x',
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                    ),
                    isEdit
                        ? InkWell(
                            onTap: () {
                              increase(index, item.price, item.discount);
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(right: 8.0),
                              child: Icon(
                                Icons.add_circle,
                                size: 17.0,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          )
                        : Container(),
                    Text(
                      item.name,
                      style: TextStyle(color: Colors.black, fontSize: 15.0),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    item.discount > 0
                        ? Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: Text(
                              '\$${round(item.price * item.amount)}',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                  decoration: TextDecoration.lineThrough),
                            ),
                          )
                        : Container(),
                    Padding(
                      padding: const EdgeInsets.only(right: 15.0),
                      child: Text(
                        '\$${round(item.price * item.amount * (100 - item.discount) / 100)}',
                        style: TextStyle(color: Colors.black, fontSize: 15.0),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text(
              item.discount > 0
                  ? '${item.discount}% off by ${item.restaurantName}'
                  : 'From ${item.restaurantName}',
              textAlign: TextAlign.start,
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
            ),
          ),
          Divider(
            height: 1.0,
          )
        ],
      ),
    );
  }

  Widget _buildMenu(List<Widget> children) {
    return Container(
      decoration: const BoxDecoration(
        //color: CupertinoColors.white,
        border: Border(
          top: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
          bottom: BorderSide(color: Color(0xFFBCBBC1), width: 0.0),
        ),
      ),
      height: 48.5,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0.0),
        child: SafeArea(
          top: false,
          bottom: false,
          child: DefaultTextStyle(
            style: const TextStyle(
              letterSpacing: -0.24,
              fontSize: 17.0,
              color: CupertinoColors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: children,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBottomPicker(Widget picker) {
    return Container(
      height: _kPickerSheetHeight,
      padding: const EdgeInsets.only(top: 6.0),
      color: CupertinoColors.white,
      child: DefaultTextStyle(
        style: const TextStyle(
          color: CupertinoColors.black,
          fontSize: 22.0,
        ),
        child: GestureDetector(
          // Blocks taps from propagating to the modal sheet and popping.
          onTap: () {},
          child: SafeArea(
            top: false,
            child: picker,
          ),
        ),
      ),
    );
  }

  Widget _buildColorPicker(BuildContext context) {
    final FixedExtentScrollController scrollController =
        FixedExtentScrollController(initialItem: _selectedColorIndex);

    return GestureDetector(
      onTap: () async {
        await showCupertinoModalPopup<void>(
          context: context,
          builder: (BuildContext context) {
            return _buildBottomPicker(
              CupertinoPicker(
                scrollController: scrollController,
                itemExtent: _kPickerItemHeight,
                backgroundColor: CupertinoColors.white,
                onSelectedItemChanged: (int index) {
                  setState(() => _selectedColorIndex = index);
                },
                children:
                    List<Widget>.generate(coolColorNames.length, (int index) {
                  return Center(
                    child: Text(coolColorNames[index]),
                  );
                }),
              ),
            );
          },
        );
      },
      child: _buildMenu(
        <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Text(
              '+ Add Delivery Time',
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.0,
                  fontWeight: FontWeight.w500),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 15.0),
            child: Text(
              'Est. ${Global.ORDER.deliveryTime} minutes ~ ${DateTime.now().add(Duration(minutes: Global.ORDER.deliveryTime)).hour}:${DateTime.now().add(Duration(minutes: Global.ORDER.deliveryTime)).minute}',
              style: TextStyle(color: Colors.black, fontSize: 15.0),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            ListView(
              children: <Widget>[
                _buildYourOrderList(),
                _buildTotal(
                    'TOTAL',
                    round(Global.ORDER.shippingFee + Global.ORDER.totalPrice),
                    true,
                    round(Global.ORDER.priceAfterDiscount +
                        Global.ORDER.shippingFee)),
                //_buildSetDeliveryTime(),
                Padding(
                  padding: const EdgeInsets.only(bottom: 50.0),
                  child: _buildColorPicker(context),
                )
              ],
            ),
            _buildFooter()
          ],
        ),
      ),
    );
  }
}

const coolColorNames = [
  "ASAP",
  "15 minutes later",
  "30 minutes later",
  "1 hour later",
  "2 hour later",
  "3 hour later",
  "5 hour later",
  "6 hour later"
];
