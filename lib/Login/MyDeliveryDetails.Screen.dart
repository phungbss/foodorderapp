import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/DeliveryAddress.Screen.dart';
import 'package:hijack/Login/YourOrder.Screen.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';

class MyDeliveryDetailsScreen extends StatefulWidget {
  @override
  _MyDeliveryDetailsScreenState createState() =>
      new _MyDeliveryDetailsScreenState();
}

class _MyDeliveryDetailsScreenState extends State<MyDeliveryDetailsScreen> {
  int paymentMethod = 0;

  gotoYourOrder() {
    confirm2();
    //confirmOrder();
    /*Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => YourOrderScreen(orderId: '',isConstruction: false,)),
    );*/
    //clearBasket();
  }

  void gotoDeliveryAddress() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DeliveryAddressScreen()),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('My Delivery Details'),
    );
  }

  //1: Order accepted
  //2: Preparing food
  //3: Order is on the way
  //4: Order delivered

  void confirm2() {
    Firestore.instance.runTransaction((trans) async {
      var items = [];
      var totalEquals = 0;

      Global.ORDER.items.forEach((item) => items.add({
            'name': item.name,
            'amount': item.amount,
            'price': item.price,
            'restaurant': item.restaurantName
          }));
      var respectsQuery = Firestore.instance
          .collection('orders')
          .document(Global.USER.uid)
          .collection('orders');
      var querySnapshot = await respectsQuery.getDocuments();
      totalEquals = querySnapshot.documents.length;

      await trans.set(
          Firestore.instance
              .collection('orders')
              .document(Global.USER.uid)
              .collection('orders')
              .document('item${totalEquals + 1}'),
          {
            'totalPrice': Global.ORDER.totalPrice,
            'shippingFee': Global.ORDER.shippingFee,
            'priceAfterDiscount': Global.ORDER.priceAfterDiscount,
            'note': Global.ORDER.note,
            'deliveryTime': Global.ORDER.deliveryTime,
            'orderTime': DateTime.now(),
            'status': 1,
            'items': items
          });

      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => YourOrderScreen(
                  orderId: 'item${totalEquals + 1}',
                  isConstruction: false,
                )),
      );
    });
  }

  confirmOrder() {
    String key = 'order' +
        DateTime.now().day.toString() +
        DateTime.now().month.toString() +
        DateTime.now().year.toString() +
        DateTime.now().hour.toString() +
        DateTime.now().minute.toString();

    var ref = Firestore.instance.collection('orders').document(Global.USER.uid);

    var items = [];

    Global.ORDER.items
        .forEach((item) => items.add({'name': item.name, 'price': item.price}));

    ref.setData({
      '1': {
        'info': {
          'totalPrice': Global.ORDER.totalPrice,
          'shippingFee': Global.ORDER.shippingFee,
          'priceAfterDiscount': Global.ORDER.priceAfterDiscount,
          'note': Global.ORDER.note,
          'deliveryTime': Global.ORDER.deliveryTime,
          'orderTime': DateTime.now(),
          'status': 1
        },
        'items': items
      }, //Order information
    }, merge: true);
  }

  Widget _buildDeliveryAddress() {
    return Container(
      padding: EdgeInsets.only(left: 15.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(color: Colors.grey.withOpacity(0.3), width: 1.0),
            top: BorderSide(color: Colors.grey.withOpacity(0.3), width: 1.0),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(top: 15.0, bottom: 10.0, right: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'DELIVERY ADDRESS',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold),
                ),
                InkWell(
                  onTap: gotoDeliveryAddress,
                  child: Text(
                    'Edit',
                    style: TextStyle(color: Colors.black, fontSize: 15.0),
                  ),
                )
              ],
            ),
          ),
          Divider(
            height: 1.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
            child: Text(
              Global.USER.address1 + ' ' + Global.USER.address2,
              style: TextStyle(
                  color: Colors.black.withOpacity(0.7),
                  fontSize: 15.0,
                  height: 1.3),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildPaymentDetails() {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            bottom: BorderSide(color: Colors.grey.withOpacity(0.3), width: 1.0),
            top: BorderSide(color: Colors.grey.withOpacity(0.3), width: 1.0),
          )),
      margin: EdgeInsets.only(top: 20.0),
      padding: EdgeInsets.only(left: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(top: 15.0, bottom: 10.0, right: 15.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'PAYMENT METHODS',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold),
                ),
                InkWell(
                  onTap: () => setState(() {}),
                  child: Text(
                    'TOTAL \$${round((Global.ORDER.priceAfterDiscount + Global.ORDER.shippingFee))}',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15.0,
                        fontWeight: FontWeight.w600),
                  ),
                )
              ],
            ),
          ),
          Divider(
            height: 1.0,
          ),

          /*CSSelection(['Cash', 'Card XXXX XXXX XXXX ' + Global.USER.card.substring(15),], (index) {

          }, currentSelection: 0),*/

          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 0;
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Icon(
                          Icons.credit_card,
                          color:
                              paymentMethod == 0 ? Colors.black : Colors.grey,
                        ),
                      ),
                      Text(
                        'XXXX XXXX XXXX ' + Global.USER.card.substring(15),
                        style: TextStyle(
                            color:
                                paymentMethod == 0 ? Colors.black : Colors.grey,
                            fontSize: 15.0,
                            height: 1.3),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: Icon(
                      Icons.check,
                      color: paymentMethod == 0
                          ? Theme.of(context).primaryColor
                          : Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Divider(
            height: 1.0,
          ),
          InkWell(
            onTap: () {
              setState(() {
                paymentMethod = 1;
              });
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Icon(
                          Icons.attach_money,
                          color:
                              paymentMethod == 1 ? Colors.black : Colors.grey,
                        ),
                      ),
                      Text(
                        'Cash',
                        style: TextStyle(
                            color:
                                paymentMethod == 1 ? Colors.black : Colors.grey,
                            fontSize: 15.0,
                            height: 1.3),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15.0),
                    child: Icon(
                      Icons.check,
                      color: paymentMethod == 1
                          ? Theme.of(context).primaryColor
                          : Colors.transparent,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFooter() {
    return FlatButton(
      onPressed: () {
        gotoYourOrder();
      },
      padding: EdgeInsets.all(0.0),
      child: Container(
        alignment: Alignment.center,
        height: 50.0,
        color: Theme.of(context).primaryColor,
        child: Text(
          'CONFIRM ORDER',
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            ListView(
              children: <Widget>[
                _buildDeliveryAddress(),
                _buildPaymentDetails()
              ],
            ),
            _buildFooter()
          ],
        ),
      ),
    );
  }
}
