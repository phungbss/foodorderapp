import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Widget/GroupInputFields.dart';

class MyDetailsScreen extends StatefulWidget {
  @override
  _MyDetailsScreenState createState() => new _MyDetailsScreenState();
}

class _MyDetailsScreenState extends State<MyDetailsScreen> {
  TextEditingController firstnameController = TextEditingController();
  TextEditingController lastnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();

  @override
  void initState() {
    super.initState();
    firstnameController.text = Global.USER.firstname;
    lastnameController.text = Global.USER.lastname;
    emailController.text = Global.USER.email;
    phoneController.text = Global.USER.phone;
  }

  goBackAndSave() {
    Firestore.instance.collection('users').document(Global.USER.uid).updateData({
      'firstname' : firstnameController.text,
      'lastname' : lastnameController.text,
      'email' : emailController.text,
      'phone' : phoneController.text,
    });
    updateUser();
    Navigator.of(context).pop();
  }

  Widget _buildAppBar() {
    return AppBar(
      leading: new IconButton(
        icon: new Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back, color: Colors.white),
        onPressed: goBackAndSave,
      ),
      title: Text('My Details'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: ListView(
            children: <Widget>[
              GroupInputFields(
                items: <Widget>[
                  GroupInputFieldsItem(hint: 'First Name', textEditingController: firstnameController,),
                  Divider(height: 1.0,),
                  GroupInputFieldsItem(hint: 'Last Name', textEditingController: lastnameController,),
                  Divider(height: 1.0,),
                  GroupInputFieldsItem(hint: 'Phone Number', textInputType: TextInputType.phone, textEditingController: phoneController,),
                  Divider(height: 1.0,),
                  GroupInputFieldsItem(hint: 'Email Address', textInputType: TextInputType.emailAddress, textEditingController: emailController,)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}