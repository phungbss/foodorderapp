import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/YourOrder.Screen.dart';
import 'package:hijack/Model/FoodOrderItem.dart';
import 'package:hijack/Model/GlobalData.dart';

class MyOrdersScreen extends StatefulWidget {
  @override
  _MyOrdersScreenState createState() => new _MyOrdersScreenState();
}

class _MyOrdersScreenState extends State<MyOrdersScreen> {
  gotoYourOrder(item, id) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => YourOrderScreen(
                status: item['status'],
                orderId: id,
              )),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('My Orders'),
    );
  }

  Widget _buildOrderItem(item, id) {
    return FlatButton(
      padding: EdgeInsets.all(0.0),
      onPressed: () => gotoYourOrder(item, id),
      child: Padding(
        padding: const EdgeInsets.only(top: 0.0, left: 8.0, right: 0.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Image(
                        width: 70.0,
                        height: 50.0,
                        fit: BoxFit.cover,
                        image: NetworkImage(
                            'https://media-cdn.tripadvisor.com/media/photo-s/11/45/43/2c/restaurant-by-night.jpg'),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4.0),
                              child: Text(
                                (item['items'][0]['name']).toString() +
                                    ' from ' +
                                    (item['items'][0]['restaurant']).toString(),
                                style: TextStyle(fontSize: 14.0),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 4.0),
                              child: Text(
                                item['orderTime'].toString(),
                                style: TextStyle(fontSize: 11.0),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(
                      Icons.arrow_forward_ios,
                      size: 12.0,
                      color: Colors.grey,
                    ),
                  )
                ],
              ),
            ),
            Divider(
              height: 0.0,
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: StreamBuilder(
            stream: Firestore.instance
                .collection('orders')
                .document(Global.USER.uid)
                .collection('orders')
                .snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return Text('Loading...');
              if (snapshot.data.documents.length > 0) {
                return Container(
                  child: ListView.builder(
                    itemBuilder: (context, index) => _buildOrderItem(
                        snapshot.data.documents[index],
                        snapshot.data.documents[index].documentID),
                    itemCount: snapshot.data.documents.length,
                  ),
                );
              } else {
                return Container(
                  alignment: Alignment.center,
                  child: Text('There\'s nothing here yet'),
                );
              }
            }),
      ),
    );
  }
}
