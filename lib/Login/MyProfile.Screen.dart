import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/PaymentDetails.Screen.dart';
import 'package:hijack/Login/Settings.Screen.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/User.dart';
import 'MyDetails.Screen.dart';
import 'DeliveryAddress.Screen.dart';
import 'PreferredLanguage.Screen.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MyProfileScreen extends StatefulWidget {
  @override
  _MyProfileScreenState createState() => new _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> {
  FirebaseAuth _auth;
  FirebaseUser _currentUser;
  Firestore firestore = Firestore.instance;


  @override
  void initState() {
    super.initState();
    _auth = FirebaseAuth.instance;
    getCurrentUser();
  }

  getCurrentUser() async {
    _currentUser = await _auth.currentUser();
  }

  refresh() {
    setState(() {});
  }

  void logOut() {
    try {
      FirebaseAuth.instance.signOut();

      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    } catch (e) {}
  }

  void gotoMyDetails() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyDetailsScreen()),
    );
  }

  void gotoPaymentDetails() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PaymentDetailsScreen()),
    );
  }

  void gotoDeliveryAddress() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DeliveryAddressScreen()),
    );
  }

  void gotoPreferredLanguage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PreferredLanguageScreen()),
    );
  }

  void gotoSettings() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SettingsScreen()),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('My Profile'),
    );
  }

  Widget _buildSettingSection(List<Widget> items) {
    return Container(
      margin: const EdgeInsets.only(top: 25.0),
      padding: const EdgeInsets.only(left: 15.0, top: 0.0, bottom: 0.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(color: Colors.grey.withAlpha(100), width: 1.0),
            bottom: BorderSide(color: Colors.grey.withAlpha(100), width: 1.0),
          )),
      child: Column(
        children: items,
      ),
    );
  }

  Widget _buildSettingItem(String text, IconData icon, Function navigation) {
    return FlatButton(
      padding: EdgeInsets.only(left: 0.0, right: 15.0),
      onPressed: navigation,
      child: Container(
        padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 5.0),
                  child: Icon(
                    icon,
                    color: Theme.of(context).primaryColor.withOpacity(0.7),
                  ),
                ),
                Text(text)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 0.0),
              child: Icon(
                Icons.arrow_forward_ios,
                color: Theme.of(context).primaryColor.withOpacity(0.7),
                size: 13.0,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: ListView(
            children: <Widget>[
              _buildSettingSection([
                _buildSettingItem(
                    Global.USER.lastname +' '+ Global.USER.firstname,
                    Icons.account_circle,
                    gotoMyDetails),
                Divider(
                  height: 1.0,
                ),
                _buildSettingItem(
                    'Payment Details', Icons.credit_card, gotoPaymentDetails),
                Divider(
                  height: 1.0,
                ),
                _buildSettingItem(
                    'Delivery Address', Icons.home, gotoDeliveryAddress)
              ]),
              _buildSettingSection([
                _buildSettingItem('Preferred Language', Icons.language,
                    gotoPreferredLanguage),
                Divider(
                  height: 1.0,
                ),
                _buildSettingItem('Settings', Icons.settings, gotoSettings),
                Divider(
                  height: 1.0,
                ),
                _buildSettingItem('Help', Icons.help, null),
                Divider(
                  height: 1.0,
                ),
                _buildSettingItem('About', Icons.info, null)
              ]),
              _buildSettingSection([
                _buildSettingItem('Log Out', Icons.exit_to_app, logOut),
              ])
            ],
          ),
        ),
      ),
    );
  }
}
