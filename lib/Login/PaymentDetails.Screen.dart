import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Widget/GroupInputFields.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Widget/MarkedTextField.dart';

class PaymentDetailsScreen extends StatefulWidget {
  @override
  _PaymentDetailsScreenState createState() => new _PaymentDetailsScreenState();
}

class _PaymentDetailsScreenState extends State<PaymentDetailsScreen> {
  TextEditingController controller = MaskedTextController(mask: '0000 0000 0000 0000');

  goBackAndSave() {
    Firestore.instance.collection('users').document(Global.USER.uid).updateData({ 'card' : controller.text });
    updateUser();
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    super.initState();
    controller.text = Global.USER.card == 'XXXX XXXX XXXX XXXX' ? '' : Global.USER.card;
  }

  Widget _buildAppBar() {
    return AppBar(
      leading: new IconButton(
        icon: new Icon(Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back, color: Colors.white),
        onPressed: goBackAndSave,
      ),
      title: Text('Payment Details'),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: ListView(
            children: <Widget>[
              GroupInputFields(
                items: <Widget>[
                  GroupInputFieldsItem(
                    hint: 'Card Number',
                    isCreditCardField: true,
                    textInputType: TextInputType.number,
                    textEditingController: controller,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}