import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Widget/CupertinoSetting.dart';

class PreferredLanguageScreen extends StatefulWidget {
  @override
  _PreferredLanguageScreenState createState() =>
      new _PreferredLanguageScreenState();
}

class _PreferredLanguageScreenState extends State<PreferredLanguageScreen> {
  Widget _buildAppBar() {
    return AppBar(
      title: Text('Preferred Language'),
    );
  }

  CSWidgetStyle brightnessStyle = const CSWidgetStyle(
      icon: const Icon(Icons.brightness_medium, color: Colors.black54));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: CupertinoSettings(<Widget>[
            new CSHeader(''),
            new CSSelection(['English', 'Chinese'], (index) {
              print(index);
            }, currentSelection: 0),
          ]
          ),
        ),
      ),
    );
  }
}
