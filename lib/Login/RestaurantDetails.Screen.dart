import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:hijack/Widget/StarRating.dart';

class RestaurantDetailsScreen extends StatefulWidget{
  _RestaurantDetailsScreenState createState() => new _RestaurantDetailsScreenState();
}

class _RestaurantDetailsScreenState extends State<RestaurantDetailsScreen> {
  Widget _buildRestaurantItem(item) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey.withOpacity(0.5), width: 0.5))
      ),
      padding: const EdgeInsets.only(top: 12.0, left: 0.0, right: 8.0, bottom: 12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(0.0),
                child: new Container(
                  margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: new Container(
                    width: 70.0,
                    height: 70.0,
                  ),
                  decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.circular(10.0),
                    color: Colors.grey,
                    image: new DecorationImage(
                        image: new NetworkImage('https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112014/pizza_hut_new_logo_2014.png?itok=KyOtuVpI'),
                        fit: BoxFit.cover),
                    boxShadow: [
                      new BoxShadow(
                          color: Colors.grey,
                          blurRadius: 5.0,
                          offset: new Offset(2.0, 5.0))
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0),
                      child: Text(
                        item['name'],
                        style: TextStyle(
                            fontSize: 17.0, fontWeight: FontWeight.w700),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0),
                      child: Text(
                        item['category'],
                        style: TextStyle(fontSize: 13.0, color: Colors.grey),
                      ),
                    ),
                    SmoothStarRating(
                      allowHalfRating: false,
                      onRatingChanged: (v) {},
                      starCount: 5,
                      rating: item['rating'],
                      size: 13.0,
                      color: Colors.amber,
                      borderColor: Colors.amber,
                    )
                  ],
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 1.0),
                color: Colors.grey.withOpacity(0.65),
                borderRadius: BorderRadius.circular(3.0)),
            child: Text(
              '45 mins',
              style: TextStyle(fontSize: 12.0),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Info'),
    );
  }
  
  Widget _buildMapView(screenWidth) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        Image(
          width: screenWidth, height: 230.0,
          fit: BoxFit.cover,
          image: NetworkImage('https://media-cdn.tripadvisor.com/media/photo-s/11/45/43/2c/restaurant-by-night.jpg'),
        ),
        BackdropFilter(
          filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
          child: Container(
            width: screenWidth - 40.0,
            padding: EdgeInsets.all(10.0),
            margin: EdgeInsets.all(20.0),
            color: Colors.white.withOpacity(0.7),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('REVELRY CAFE', style: TextStyle(fontSize: 16.0),),
                Text('21, Lorong Kilat #01-02. Singapore 598484', style: TextStyle(color: Colors.grey))
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget _buildRating() {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.grey, width: 0.7))
      ),
      padding: EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('AVERAGE RATING', style: TextStyle(fontSize: 16.0),),
          Row(
            children: <Widget>[
              SmoothStarRating(
                allowHalfRating: false,
                onRatingChanged: (v) {},
                starCount: 5,
                rating: item['rating'],
                size: 20.0,
                color: Colors.amber,
                borderColor: Colors.amber,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text('(15)', style: TextStyle(fontSize: 16.0),),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildOpeningHours() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('OPENING HOURS', style: TextStyle(fontSize: 16.0),),

        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    
    
    return Scaffold(
      appBar: _buildAppBar(),
      body: ListView(
        children: <Widget>[
          _buildRestaurantItem(item),
          _buildMapView(screenWidth),
          _buildRating(),
          _buildOpeningHours()
        ],
      ),
    );
  }
}

const item = {
  'id': 1,
  'name': 'PIZZA HUT',
  'category': 'Salad, Western',
  'rating': 4.0
};