import 'dart:ui';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:hijack/Login/MyBasket.Screen.dart';
import 'package:hijack/Login/RestaurantDetails.Screen.dart';
import 'package:hijack/Model/FoodOrderItem.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Model/Restaurant.dart';
import 'package:hijack/Widget/StarRating.dart';

class RestaurantMenuScreen extends StatefulWidget {
  final Restaurant restaurant;

  const RestaurantMenuScreen({Key key, this.restaurant}) : super(key: key);

  @override
  _RestaurantMenuScreenState createState() =>
      new _RestaurantMenuScreenState(restaurant);
}

class _RestaurantMenuScreenState extends State<RestaurantMenuScreen> {
  Restaurant restaurant;
  int sharedValue = 0;


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  final Map<int, Widget> children = const <int, Widget>{
    0: Text(
      'Food',
      style: TextStyle(fontSize: 13.0),
    ),
    1: Text(
      'Drink',
      style: TextStyle(fontSize: 13.0),
    ),
  };

  _RestaurantMenuScreenState(Restaurant r) {
    this.restaurant = r;
  }

  refreshUI() {
    setState(() {});
  }

  /*void getRestaurantInfo() {
    Firestore.instance
        .collection('restaurants')
        .document(restaurant.id)
        .get()
        .then((data) => photoUrl = data['photoUrl']).whenComplete(refreshUI);
  }*/

  gotoDetailsScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RestaurantDetailsScreen()),
    );
  }

  gotoBasketScreen() {
    if (Global.ORDER.items.length == 0) {
      showSnackBar(
          'Please select something first, maybe something to drink...');
    } else
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MyBasketScreen()),
      );
  }

  showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Text(text),
      ),
      duration: Duration(seconds: 1),
      backgroundColor: Theme.of(context).primaryColor,
    ));
  }

  bool isThisShitAlreadyOnTheList(String foodID, String restaurantID) {
    for (int i = 0; i < Global.ORDER.items.length; i++) {
      if (Global.ORDER.items[i].foodID == foodID &&
          Global.ORDER.items[i].restaurantID == restaurantID) {
        return true;
      }
    }
    return false;
  }

  void deleteThisShitFromTheList(String foodID, String restaurantID) {
    for (int i = 0; i < Global.ORDER.items.length; i++) {
      if (Global.ORDER.items[i].foodID == foodID &&
          Global.ORDER.items[i].restaurantID == restaurantID) {
        Global.ORDER.totalPrice = round(Global.ORDER.totalPrice -
            Global.ORDER.items[i].amount * Global.ORDER.items[i].price);
        Global.ORDER.priceAfterDiscount = round(Global.ORDER.priceAfterDiscount -
            Global.ORDER.items[i].amount * Global.ORDER.items[i].price * (100 - Global.ORDER.items[i].discount) / 100);
        Global.ORDER.items.removeAt(i);
        Global.ORDER.deliveryTime = Global.ORDER.items.map((i) => i.deliveryTime).toList().reduce(max);
      }
    }
  }

  Widget _buildAppBar() {
    return AppBar(
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.info_outline), onPressed: gotoDetailsScreen)
        ],
        title: SizedBox(
          width: 200.0,
          height: 28.0,
          child: CupertinoSegmentedControl<int>(
            children: children,
            borderColor: Colors.white,
            pressedColor: Colors.transparent,
            selectedColor: Colors.white,
            unselectedColor: Theme.of(context).primaryColor,
            onValueChanged: (int newValue) {
              setState(() {
                sharedValue = newValue;
              });
            },
            groupValue: sharedValue,
          ),
        ));
  }

  Widget _buildBanner(double screenWidth) {
    return Stack(
      children: <Widget>[
        SizedBox(
          height: 200.0,
          child: Image(
            image: NetworkImage(restaurant.photoUrl),
            fit: BoxFit.cover,
            width: screenWidth,
          ),
        ),
        Container(
          height: 200.0,
          alignment: Alignment.center,
          color: Colors.grey.withOpacity(0.7),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 16.0, right: 16.0),
                child: new Container(
                  width: 70.0,
                  height: 70.0,
                ),
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.circular(10.0),
                  color: Colors.grey,
                  image: new DecorationImage(
                      image: new NetworkImage(restaurant.avatarUrl),
                      fit: BoxFit.cover),
                  boxShadow: [
                    new BoxShadow(
                        color: Colors.grey,
                        blurRadius: 5.0,
                        offset: new Offset(2.0, 5.0))
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text(
                  restaurant.name,
                  style: TextStyle(
                      fontSize: 28.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w700),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text(
                  restaurant.category,
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ),
              ),
              SmoothStarRating(
                allowHalfRating: true,
                onRatingChanged: (v) {},
                starCount: 5,
                rating: restaurant.rating,
                size: 16.0,
                color: Colors.amber,
                borderColor: Colors.amber,
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _buildInfoSegment(double screenWidth) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            alignment: Alignment.center,
            width: restaurant.discount > 0 ? screenWidth / 3 : screenWidth / 2,
            decoration: BoxDecoration(
                border: Border(
                    right: BorderSide(color: Colors.grey, width: 1.0),
                    bottom: BorderSide(color: Colors.grey, width: 1.0))),
            child: Text(
              'Min order: \$${restaurant.minOrder}',
              style: TextStyle(fontSize: 11.0),
            )),
        Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            alignment: Alignment.center,
            width: restaurant.discount > 0 ? screenWidth / 3 : screenWidth / 2,
            decoration: BoxDecoration(
                border: Border(
                    right: restaurant.discount > 0 ? BorderSide(color: Colors.grey, width: 1.0) : BorderSide.none,
                    bottom: BorderSide(color: Colors.grey, width: 1.0))),
            child: Text( 'Est. ${restaurant.estDeliveryTime} minutes',
                style: TextStyle(fontSize: 11.0))),
        restaurant.discount > 0 ? Container(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
            alignment: Alignment.center,
            width: screenWidth / 3,
            decoration: BoxDecoration(
                border:
                Border(bottom: BorderSide(color: Colors.grey, width: 1.0))),
            child: Text('${restaurant.discount}% off',
                style: TextStyle(fontSize: 11.0))) : Container(),
      ],
    );
  }

  Widget _buildFooter() {
    return FlatButton(
      padding: EdgeInsets.all(0.0),
      onPressed: gotoBasketScreen,
      child: BackdropFilter(
        filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
        child: Container(
          height: 48.0,
          color: Theme.of(context).primaryColor.withOpacity(0.7),
          padding: EdgeInsets.only(left: 15.0, right: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Icon(Global.ORDER.items.length > 0 ? Icons.shopping_basket : Icons.fastfood, color: Colors.white),
                  ),
                  Text(
                    Global.ORDER.items.length > 0 ? Global.ORDER.items.length.toString() + ' item(s)' : 'What would you like for lunch?',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  )
                ],
              ),
              Global.ORDER.totalPrice > 0.0 ? Row(
                children: <Widget>[
                  Text(
                    'TOTAL ',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                  Global.ORDER.totalPrice != Global.ORDER.priceAfterDiscount ? Text(
                    '\$${Global.ORDER.totalPrice}',
                    style: TextStyle(color: Colors.white, fontSize: 16.0, decoration: TextDecoration.lineThrough),
                  ) : Container(),
                  Text(
                    ' \$${Global.ORDER.priceAfterDiscount}',
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ],
              ) : Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildHeader(String name) {
    return BackdropFilter(
      filter: new ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Container(
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5)),
          color: Colors.white.withOpacity(0.5),
        ),
        padding: EdgeInsets.only(left: 15.0, top: 8.0, bottom: 8.0),
        child: Text(
          name,
          style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w700),
        ),
      ),
    );
  }

  Widget _buildFoodItem(item, index) {
    return FlatButton(
      onPressed: () {
        if (isThisShitAlreadyOnTheList(index, restaurant.id)) {
          deleteThisShitFromTheList(index, restaurant.id);
          showSnackBar('Removed ${item['name']} from your basket');
        } else {
          showSnackBar('Added ${item['name']} to your basket');
          Global.ORDER.items.add(FoodOrderItem(
              index, restaurant.id, item['name'], item['price'], 1, restaurant.discount, restaurant.name, restaurant.estDeliveryTime));
          Global.ORDER.totalPrice = round(Global.ORDER.totalPrice + item['price'] * 1.0);
          Global.ORDER.priceAfterDiscount = round(Global.ORDER.priceAfterDiscount + item['price'] * 1.0 * (100 - restaurant.discount) / 100);
          Global.ORDER.deliveryTime = restaurant.estDeliveryTime > Global.ORDER.deliveryTime ? restaurant.estDeliveryTime : Global.ORDER.deliveryTime;
        }
        refreshUI();
      },
      padding: EdgeInsets.all(0.0),
      child: Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey, width: 0.5)),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Image(
              width: 60.0,
              height: 60.0,
              fit: BoxFit.cover,
              image: NetworkImage(item['photoUrl']),
            ),
            Expanded(
              child: Container(
                height: 60.0,
                padding: const EdgeInsets.only(left: 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text(item['name'],
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16.0,
                                )),
                            isThisShitAlreadyOnTheList(index, restaurant.id) ? Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Icon(
                                    Icons.shopping_basket,
                                    color: Theme.of(context).primaryColor,
                                    size: 14.0,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 8.0),
                                  child: Text('${restaurant.discount}% off', style: TextStyle(color: restaurant.discount > 0 ? Colors.grey : Colors.transparent),),
                                )
                              ],
                            ) : Container(),

                          ],
                        ),
                        Text('\$' + item['price'].toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.w700, fontSize: 14.0))
                      ],
                    ),
                    Text(
                      item['description'],
                      style: TextStyle(fontSize: 12.0),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: _buildAppBar(),
      body: Container(
          width: double.infinity,
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              /*sharedValue == 0 ? StickyList(
                children: <StickyListRow>[
                  HeaderRow(child: Container()),
                  RegularRow(child: _buildBanner(screenWidth)),
                  RegularRow(child: _buildInfoSegment(screenWidth)),
                  HeaderRow(child: _buildHeader('WAFFLES')),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  HeaderRow(child: _buildHeader('PIZZA')),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                ]
            ) : StickyList(
                children: <StickyListRow>[
                  HeaderRow(child: Container()),
                  RegularRow(child: _buildBanner(screenWidth)),
                  RegularRow(child: _buildInfoSegment(screenWidth)),
                  HeaderRow(child: _buildHeader('JUICE')),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  HeaderRow(child: _buildHeader('MILKSHAKE')),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                  RegularRow(child: _buildFoodItem()),
                ]
            ),*/
              ListView(
                children: <Widget>[
                  _buildBanner(screenWidth),
                  _buildInfoSegment(screenWidth),
                  sharedValue == 0
                      ? StreamBuilder(
                          stream: Firestore.instance
                              .collection('menus')
                              .document(restaurant.id)
                              .collection('food')
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) return Text('Loading...');
                            return Container(
                              height: screenHeight - 110.0,
                              child: ListView.builder(
                                //physics: ClampingScrollPhysics(),
                                itemBuilder: (context, index) => _buildFoodItem(
                                    snapshot.data.documents[index],
                                    snapshot.data.documents[index].documentID),
                                itemCount: snapshot.data.documents.length,
                              ),
                            );
                          })
                      : StreamBuilder(
                          stream: Firestore.instance
                              .collection('menus')
                              .document(restaurant.id)
                              .collection('drink')
                              .snapshots(),
                          builder: (context, snapshot) {
                            if (!snapshot.hasData) return Text('Loading...');
                            return Container(
                              height: screenHeight - 110.0,
                              child: ListView.builder(
                                //physics: ClampingScrollPhysics(),
                                itemBuilder: (context, index) => _buildFoodItem(
                                    snapshot.data.documents[index],
                                    snapshot.data.documents[index].documentID),
                                itemCount: snapshot.data.documents.length,
                              ),
                            );
                          }),
                ],
              ),
              _buildFooter()
            ],
          )),
    );
  }
}