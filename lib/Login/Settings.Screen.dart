import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Widget/CupertinoSetting.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => new _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool isReceivingPush = false;
  bool isReceivingNewsletter = false;

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Preferred Language'),
    );
  }

  CSWidgetStyle brightnessStyle = const CSWidgetStyle(
      icon: const Icon(Icons.brightness_medium, color: Colors.black54));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        width: double.infinity,
        child: Container(
          color: Colors.grey.withOpacity(0.25),
          child: CupertinoSettings(<Widget>[
            CSHeader(''),
            CSControl(
              'Receive Push Notification',
              CupertinoSwitch(
                activeColor: Theme.of(context).primaryColor,
                value: isReceivingPush,
                onChanged: (bool value) {
                  setState(() {
                    isReceivingPush = value;
                  });
                },
              ),
            ),
            CSControl(
              'Receive Newsletter',
              CupertinoSwitch(
                activeColor: Theme.of(context).primaryColor,
                value: isReceivingNewsletter,
                onChanged: (bool value) {
                  setState(() {
                    isReceivingNewsletter = value;
                  });
                },
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
