import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Model/User.dart';
import 'LocationQuestionnaire.Screen.dart';
import 'Login.Screen.dart';

import 'package:firebase_auth/firebase_auth.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => new _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String _firstName = '';
  String _lastName = '';
  String _email = '';
  String _password = '';
  String _mobileNumber = '';
  String _confirmPassword = '';

  FirebaseUser user;

  void onPressSignUp() async {
    try {
      user = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: _email, password: _password);

      Firestore.instance.runTransaction((trans) async {
        await trans
            .set(Firestore.instance.collection('users').document(user.uid), {
          'firstname': _firstName,
          'lastname': _lastName,
          'email': _email,
          'phone': '(+84) ' + _mobileNumber,
          'card': 'XXXX XXXX XXXX XXXX',
          'address1': '',
          'address2': '',
          'postcode': '',
        });
      });

      updateUser();

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LocationQuestionnaireScreen()),
      );
    } catch (e) {}
  }

  void onPressLogInLink() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Sign Up'),
    );
  }

  Widget _buildForm() {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
        child: ListView(
          children: <Widget>[
            Container(
              child: TextField(
                  onChanged: (t) => _firstName = t,
                  decoration: InputDecoration(labelText: 'First Name')),
            ),
            TextField(
                onChanged: (t) => _lastName = t,
                decoration: InputDecoration(labelText: 'Last Name')),
            TextField(
                keyboardType: TextInputType.emailAddress,
                onChanged: (t) => _email = t,
                decoration: InputDecoration(labelText: 'Email Address')),
            TextField(
                keyboardType: TextInputType.phone,
                onChanged: (t) => _mobileNumber = t,
                decoration: InputDecoration(
                    labelText: 'Mobile Number',
                    prefix: Container(
                        margin: EdgeInsets.only(right: 8.0),
                        child: Text('+84')))),
            TextField(
                obscureText: true,
                onChanged: (t) => _password = t,
                decoration: InputDecoration(labelText: 'Password')),
            TextField(
              obscureText: true,
              onChanged: (t) => _confirmPassword = t,
              decoration: InputDecoration(labelText: 'Confirm Password'),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Padding(
      padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 15.0),
      child: RaisedButton(
        onPressed: onPressSignUp,
        textColor: Colors.white,
        color: Theme.of(context).primaryColor,
        child: Container(
          alignment: Alignment.center,
          height: 45.0,
          child: new Text(
            "SIGN UP",
          ),
        ),
      ),
    );
  }

  Widget _buildLinks() {
    return Column(
      children: <Widget>[
        Container(
          height: 25.0,
          margin: EdgeInsets.only(bottom: 10.0),
          child: FlatButton(
            onPressed: onPressLogInLink,
            child: Text('Already have an account? Log In!'),
          ),
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        appBar: _buildAppBar(),
        body: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              _buildForm(),
              //Spacer(),
              _buildLinks(),
              _buildButton()
            ],
          ),
        ),
      ),
    );
  }
}
