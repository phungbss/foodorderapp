import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Login/Home.Screen.dart';
import 'package:hijack/Login/Login.Screen.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:hijack/Login/SignUp.Screen.dart';
import 'package:hijack/Model/FoodOrderItem.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/Order.dart';
import 'package:hijack/Model/User.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => new _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  FirebaseAuth _auth;
  FirebaseUser _currentUser;
  Firestore firestore = Firestore.instance;


  void createRes() {
    var r = new Random();
    Firestore.instance.runTransaction((trans) async {
      for (int i = 0; i < 10; i++) {
        await trans.set(
            Firestore.instance
                .collection('restaurants')
                .document('restaurant${i}'),
            {
              'name': 'Pizza Hut No. ${i}',
              'category': 'Pizza, fastfood',
              'time': r.nextInt(50),
              'rating': r.nextInt(10) * 0.5,
              'discount': r.nextInt(30),
              'minOrder': 9.99,
              'avatarUrl':
                  'https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112014/pizza_hut_new_logo_2014.png?itok=KyOtuVpI',
              'photoUrl':
                  'https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112014/pizza_hut_new_logo_2014.png?itok=KyOtuVpI'
            });
      }
    });
  }

  void createMenus() {
    var r = new Random();
    Firestore.instance.runTransaction((trans) async {
      for (int j = 0; j < 10; j++) {
        for (int i = 0; i < 20; i++) {

          await trans.set(
              Firestore.instance
                  .collection('menus')
                  .document('restaurant${j}')
                  .collection('drink')
                  .document('drink${i}'),
              {
                'name': 'Drink no. ${i}',
                'price': double.parse((0.99 * r.nextInt(20)).toStringAsFixed(2)),
                'photoUrl': 'https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112014/pizza_hut_new_logo_2014.png?itok=KyOtuVpI',
                'description': 'Description placeholder'
              });

          await trans.set(
              Firestore.instance
                  .collection('menus')
                  .document('restaurant${j}')
                  .collection('food')
                  .document('food${i}'),
              {
                'name': 'Food no. ${i}',
                'price': double.parse((0.99 * r.nextInt(20)).toStringAsFixed(2)),
                'photoUrl': 'https://botw-pd.s3.amazonaws.com/styles/logo-thumbnail/s3/112014/pizza_hut_new_logo_2014.png?itok=KyOtuVpI',
                'description': 'Description placeholder'
              });
        }
      }
    });
  }

  void onPressLoginBtn() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }

  void onPressSignUpBtn() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SignUpScreen()),
    );
  }

  getCurrentUser() async {
    _currentUser = await _auth.currentUser();

    if (_currentUser != null) {
      firestore
          .collection('users')
          .document(_currentUser.uid)
          .get()
          .then((data) => Global.USER = User(
                _currentUser.uid,
                data['firstname'],
                data['lastname'],
                data['email'],
                data['phone'],
                data['card'],
                data['address1'],
                data['address2'],
                data['postcode'],
              ));

      /*Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HomeScreen()),
      );*/

      Navigator.of(context)
          .pushNamedAndRemoveUntil('/home', (Route<dynamic> route) => false);
    }
  }

  @override
  void initState() {
    super.initState();
    _auth = FirebaseAuth.instance;
    getCurrentUser();

    List<FoodOrderItem> items = [];
    Global.ORDER = new Order(items);

    //createRes();
    //createMenus();
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                'https://images.homedepot-static.com/productImages/524fdd32-0c0f-4cf9-ac21-ecdcfcca038c/svn/kenneth-james-wallpaper-2671-22409-64_1000.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: <Widget>[
            Spacer(),
            Padding(
              padding:
                  const EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  RaisedButton(
                    onPressed: onPressLoginBtn,
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Container(
                      alignment: Alignment.center,
                      width: screenWidth * 0.35,
                      height: 45.0,
                      child: new Text(
                        "LOG IN",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                  RaisedButton(
                    onPressed: onPressSignUpBtn,
                    textColor: Colors.white,
                    color: Theme.of(context).primaryColor,
                    child: Container(
                      alignment: Alignment.center,
                      width: screenWidth * 0.35,
                      height: 45.0,
                      child: new Text(
                        "SIGN UP",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 10.0),
              child: RaisedButton(
                onPressed: onPressLoginBtn,
                textColor: Colors.white,
                color: Theme.of(context).primaryColor,
                child: Container(
                  alignment: Alignment.center,
                  height: 45.0,
                  child: new Text(
                    "LOGIN WITH FACEBOOK",
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
