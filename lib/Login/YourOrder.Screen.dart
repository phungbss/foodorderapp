import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hijack/Widget/StarRating.dart';
import 'package:hijack/Model/FoodOrderItem.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/GlobalFunction.dart';
import 'package:hijack/Model/Order.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class YourOrderScreen extends StatefulWidget {
  final String orderId;
  final int status;
  final bool isConstruction;
  const YourOrderScreen(
      {Key key, this.orderId, this.status, this.isConstruction})
      : super(key: key);
  @override
  _YourOrderScreenState createState() =>
      new _YourOrderScreenState(orderId, status, isConstruction);
}

class _YourOrderScreenState extends State<YourOrderScreen> {
  String id;
  bool isConstruction = false;
  double _shippingFee = 0;
  double _totalPrice = 0;
  double _subTotal = 0;
  double _priceAfterDiscount = 0;
  bool isDone = false;
  double rating = 0.0;
  int status = 0;
  int time = 0;
  var foods = [];

  Firestore firestore = Firestore.instance;

  @override
  void dispose() {
    clearBasket();
    super.dispose();
  }

  _YourOrderScreenState(id, status, isConstrucion) {
    this.id = id;
    this.isConstruction = isConstruction;
    //this.status = status;

    firestore
        .collection('orders')
        .document(Global.USER.uid)
        .collection('orders')
        .document(id)
        .get()
        .then((data) => setData(data))
        .then((data) => refreshUI());
  }

  setData(data) {
    status = data['status'];
    time = data['deliveryTime'];
    foods = data['items'];
    _subTotal = data['totalPrice'];
    _priceAfterDiscount = data['priceAfterDiscount'];
    _shippingFee = data['shippingFee'];
  }

  refreshUI() {
    setState(() {});
  }

  Widget _buildAppBar() {
    return AppBar(
      title: Text('Your Order'),
    );
  }

  _buildEST() {
    return Container(
      margin: EdgeInsets.all(22.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Icon(
              Icons.fastfood,
              size: 70.0,
            ),
          ),
          Text(
            status != 4 ? time.toString() + ' mins' : 'Delivered',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40.0),
          ),
          Text(
            'Estimated Delivery Time',
            style: TextStyle(fontWeight: FontWeight.normal, fontSize: 13.0),
          )
        ],
      ),
    );
  }

  _buildProgressStep(IconData icon, String text1, String text2,
      {isLastOne = false, isFinished = false}) {
    return Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: isLastOne
                      ? Colors.transparent
                      : Colors.grey.withOpacity(0.5),
                  width: 1.0))),
      padding: EdgeInsets.all(20.0),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(
              icon,
              size: 35.0,
              color: isFinished ? Colors.black : Colors.black.withOpacity(0.5),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Text(
                  text1,
                  style: TextStyle(
                      color: isFinished
                          ? Colors.black
                          : Colors.black.withOpacity(0.5)),
                ),
              ),
              Text(
                text2,
                style: TextStyle(
                    fontSize: 12.0, color: Colors.black.withOpacity(0.5)),
              ),
            ],
          )
        ],
      ),
    );
  }

  _buildProgress() {
    return Container(
      margin: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: Column(
        children: <Widget>[
          _buildProgressStep(Icons.check_circle_outline, 'Order accepted',
              'Your oder has been accepted',
              isFinished: this.status >= 1),
          _buildProgressStep(Icons.pan_tool, 'Preparing food',
              'Your food is currently being prepared',
              isFinished: this.status >= 2),
          _buildProgressStep(Icons.card_travel, 'Order is on the way',
              'Your food is on the way',
              isFinished: this.status >= 3),
          _buildProgressStep(Icons.event_seat, 'Order delivered',
              'Your order was delivered, enjoy your meal!',
              isFinished: this.status == 4, isLastOne: true)
        ],
      ),
    );
  }

  Widget _buildOrderItem(amount, name, price) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        amount.toString() + 'x',
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                            fontSize: 13.0),
                      ),
                    ),
                    Text(
                      name,
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.5), fontSize: 13.0),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 0.0),
                  child: Text(
                    '\$' + (amount * price).toString(),
                    style: TextStyle(
                        color: Colors.black.withOpacity(0.5), fontSize: 13.0),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildOrderItem2(item) {
    _shippingFee = item['shippingFee'];
    _priceAfterDiscount = item['priceAfterDiscount'];
    _subTotal = item['totalPrice'];
    _totalPrice = item['priceAfterDiscount'] + item['shippingFee'];

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Text(
                        item['items']['amount'].toString() + 'x',
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.5),
                            fontSize: 13.0),
                      ),
                    ),
                    Text(
                      item['items']['name'],
                      style: TextStyle(
                          color: Colors.black.withOpacity(0.5), fontSize: 13.0),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 0.0),
                  child: Text(
                    '\$' +
                        (item['items']['amount'] * item['items']['price'])
                            .toString(),
                    style: TextStyle(
                        color: Colors.black.withOpacity(0.5), fontSize: 13.0),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFeeItem(String text, double amount) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 10.0, left: 10.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: 0.0),
                  child: Text(
                    text,
                    style: TextStyle(color: Colors.black, fontSize: 13.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 0.0),
                  child: Text(
                    '\$' + amount.toStringAsFixed(2),
                    style: TextStyle(color: Colors.black, fontSize: 13.0),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _buildThankYouNote() {
    return Container(
      margin: EdgeInsets.all(10.0),
      padding:
          EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0, right: 10.0),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: Column(
          children: new List.generate(foods.length, (int index) {
        return _buildOrderItem(foods[index]['amount'], foods[index]['name'],
            foods[index]['price']);
      })),
      //_buildOrderItem(),
      // _buildOrderItem(4, 'Salmon Salad', 14.5),
      // _buildOrderItem(3, 'Pho' , 16.5),
    );
  }

  _buildThankYouNote2() {
    return Container(
      margin: EdgeInsets.all(10.0),
      padding:
          EdgeInsets.only(top: 15.0, bottom: 15.0, left: 10.0, right: 10.0),
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.3),
          borderRadius: BorderRadius.all(Radius.circular(5.0))),
      child: StreamBuilder(
          stream: Firestore.instance
              .collection('orders')
              .document(Global.USER.uid)
              .collection('orders')
              .document(id)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Text('Loading...');
            return Container(
              child: ListView.builder(
                  itemBuilder: (context, index) => _buildOrderItem2(
                        snapshot.data.documents[index],
                      ),
                  itemCount: snapshot.data.documents.length),
            );
          }),
    );
  }

  _buildRatingSection(double screenWidth) {
    return Container(
      margin: EdgeInsets.only(top: 22.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              'Hope you enjoyed your meal',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              'Share your experience',
              style: TextStyle(fontWeight: FontWeight.normal, fontSize: 13.0),
            ),
          ),
          SmoothStarRating(
            allowHalfRating: true,
            onRatingChanged: (v) {
              setState(() {
                rating = v;
              });
            },
            starCount: 5,
            rating: rating,
            size: 40.0,
            color: Colors.amber,
            borderColor: Colors.amber,
          ),
          /*Container(
            width: screenWidth - 20.0,
            margin: EdgeInsets.only(top: 15.0),
            padding: EdgeInsets.all(0.0),
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor.withOpacity(0.7),
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: FlatButton(
              onPressed: () {},
              child: Column(
                children: <Widget>[
                  Text(
                    'REPEAT ORDER',
                    style: TextStyle(fontSize: 20.0, color: Colors.white),
                  ),
                ],
              ),
            ),
          )*/
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: _buildAppBar(),
      body: Container(
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            StreamBuilder(
                stream: Firestore.instance
                    .collection('orders')
                    .document(Global.USER.uid)
                    .collection('orders')
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) return Text('Loading...');
                  if (snapshot.data.documents.length > 0) {
                    firestore
                        .collection('orders')
                        .document(Global.USER.uid)
                        .collection('orders')
                        .document(id)
                        .get()
                        .then((data) => setData(data))
                        .then((data) => refreshUI());

                    return ListView(
                      children: <Widget>[
                        status != 4
                            ? _buildEST()
                            : _buildRatingSection(screenWidth),
                        status != 4 ? _buildProgress() : Container(),
                        Container(
                          padding: const EdgeInsets.only(bottom: 5.0),
                          child: Text(
                            'Thank you for your order',
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Text(foods.length.toString() + ' item(s)',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 12.0,
                                color: Colors.black.withOpacity(0.5))),
                        Divider(),
                        isConstruction
                            ? _buildThankYouNote2()
                            : _buildThankYouNote(),
                        _buildFeeItem('Subtotal', _subTotal),
                        _buildFeeItem(
                            'Price after discount', _priceAfterDiscount),
                        _buildFeeItem('Delivery Fee', _shippingFee),
                        _buildFeeItem(
                            'TOTAL', _priceAfterDiscount + _shippingFee),
                      ],
                    );
                  } else {
                    return Container(
                      alignment: Alignment.center,
                      child: Text('There\'s nothing here yet'),
                    );
                  }
                }),
          ],
        ),
      ),
    );
  }
}
