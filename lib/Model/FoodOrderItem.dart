class FoodOrderItem {
  final String foodID;
  final String restaurantID;
  final String name;
  final double price;
  int amount;
  int discount;
  String restaurantName;
  int deliveryTime;

  FoodOrderItem(this.foodID, this.restaurantID, this.name, this.price,
      this.amount, this.discount, this.restaurantName, this.deliveryTime);
}