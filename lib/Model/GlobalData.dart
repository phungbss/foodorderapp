import 'package:hijack/Model/Order.dart';
import 'package:hijack/Model/User.dart';

class Global {
  static User USER;
  static Order ORDER;
}