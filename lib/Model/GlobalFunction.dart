import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hijack/Model/GlobalData.dart';
import 'package:hijack/Model/User.dart';

void updateUser() {
  Firestore.instance
      .collection('users')
      .document(Global.USER.uid)
      .get()
      .then((data) => Global.USER = User(
            Global.USER.uid,
            data['firstname'],
            data['lastname'],
            data['email'],
            data['phone'],
            data['card'],
            data['address1'],
            data['address2'],
            data['postcode'],
          ));
}

double round(double num) {
  return double.parse(num.toStringAsFixed(2));
}

clearBasket() {
  Global.ORDER.items.clear();
  Global.ORDER.totalPrice = 0.0;
  Global.ORDER.priceAfterDiscount = 0.0;
  Global.ORDER.note = '';
  Global.ORDER.deliveryTime = 0;
}
