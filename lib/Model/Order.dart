import 'package:hijack/Model/FoodOrderItem.dart';

class Order {
  final List<FoodOrderItem> items;
  double totalPrice = 0.0;
  double shippingFee = 3.0;
  double priceAfterDiscount = 0.0;
  String note = '';
  int deliveryTime = 0;
  String id='';
  Order(this.items);
}