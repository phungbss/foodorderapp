class Restaurant {
  final String id;
  final String name;
  final String category;
  final double rating;
  final int estDeliveryTime;
  final String avatarUrl;
  final String photoUrl;
  final int discount;
  final double minOrder;

  Restaurant(this.id, this.name, this.category, this.rating, this.estDeliveryTime, this.avatarUrl, this.photoUrl, this.discount, this.minOrder);
}