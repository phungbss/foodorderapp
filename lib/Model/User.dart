class User {
  final String uid;
  final String firstname;
  final String lastname;
  final String email;
  final String phone;
  final String card;
  final String address1;
  final String address2;
  final String postcode;

  User(this.uid, this.firstname, this.lastname, this.email, this.phone, this.card, this.address1, this.address2, this.postcode);
}