import 'package:flutter/material.dart';

import 'orderitem.dart';

var id;

class HomeScreenV2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreenV2> {

  final List<OrderItem> _items = <OrderItem>[
    new OrderItem(1, 'Mixed Grill', 'Platter', 1, 30.0, 'mixedgrill.jpg'),
    new OrderItem(
        2, 'Grilled Chicken', 'Sandwich', 2, 10.0, 'chickensandwich.jpg'),
    new OrderItem(3, 'Fresh Orange Juice', 'Drink', 3, 8.0, 'orangejuice.jpg'),
    new OrderItem(4, 'Fresh Apple Juice', 'Drink', 1, 8.0, 'applejuice.jpg'),
  ];

  static final TextStyle _boldStyle =
  new TextStyle(fontWeight: FontWeight.bold);
  static final TextStyle _greyStyle = new TextStyle(color: Colors.grey);

  final ddlValues = <int>[1, 2, 3, 4];


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      body: new CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          new SliverAppBar(
            backgroundColor: Theme.of(context).primaryColor,
            expandedHeight: 180.0,
            pinned: true,
            actions: <Widget>[
              IconButton(icon: Icon(Icons.info_outline), onPressed: null, color: Colors.white,)
            ],
            flexibleSpace: new FlexibleSpaceBar(
              title: const Text("Shizuka Restaurant", textAlign: TextAlign.start,),
              background: Image(
                fit: BoxFit.cover,
                image: NetworkImage('https://media-cdn.tripadvisor.com/media/photo-s/11/45/43/2c/restaurant-by-night.jpg'),
              ),
            ),
          ),
          new SliverPadding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
              sliver: new SliverFixedExtentList(
                itemExtent: 172.0,
                delegate: new SliverChildBuilderDelegate(
                        (builder, index) => null,
                    childCount: _items.length),
              )),
        ],
      ),
    );
  }
}