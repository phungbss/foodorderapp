import 'package:flutter/material.dart';
import 'package:hijack/Real/Restaurant.Screen.dart';
import 'package:hijack/Widget/StarRating.dart';

class HomeV2Screen extends StatefulWidget {
  _HomeV2ScreenState createState() => new _HomeV2ScreenState();
}

class _HomeV2ScreenState extends State<HomeV2Screen> {
  void gotoRestaurantDetailsScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PestoHome()),
    );
  }

  Widget _buildRestaurantItem() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Image(
          width: 70.0, height: 70.0,
          fit: BoxFit.cover,
          image: NetworkImage('https://marketplace.canva.com/MACP0--HhzM/1/0/thumbnail_large/canva-black-circle-with-utensils-restaurant-logo-MACP0--HhzM.jpg'),
        ),
        Container(
          height: 70.0,
          padding: const EdgeInsets.only(left: 8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text('Shizuka Restaurant', style: TextStyle(fontSize: 17.0, color: Colors.white),),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: Text('Bakery | Salads | Western', style: TextStyle(fontSize: 13.0, color: Colors.white),),
              ),
              SmoothStarRating(
                allowHalfRating: false,
                onRatingChanged: (v) {},
                starCount: 5,
                rating: 4.0,
                size: 15.0,
                color: Colors.amber,
                borderColor: Colors.amber,
              )
            ],
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;

    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              title: Text(''),
              expandedHeight: 200.0,
              floating: false,
              pinned: true,
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: gotoRestaurantDetailsScreen,
              ),
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.info_outline),
                  onPressed: gotoRestaurantDetailsScreen,
                ),
              ],
              flexibleSpace: FlexibleSpaceBar(
                  background: Container(
                    width: screenSize.width,
                    padding: EdgeInsets.only(left: 15.0, right: 15.0, top: 90.0),
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                        image: new NetworkImage("https://media-cdn.tripadvisor.com/media/photo-s/11/45/43/2c/restaurant-by-night.jpg"),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        _buildRestaurantItem(),
                        Container(
                          height: 46.0,
                          margin: EdgeInsets.only(top: 14.0),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(6.0)),
                              color: Colors.white,
                              boxShadow: [new BoxShadow(
                                color: Colors.grey,
                                blurRadius: 10.0,
                              ),]
                          ),
                          child: TextField(
                            decoration: InputDecoration(
                                border: InputBorder.none,
                                prefixIcon: Icon(Icons.search),
                                hintText: 'Search for dishes...'
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
              ),
            ),
          ];
        },
        body: Center(
          child: Text("Sample Text"),
        ),
      ),
    );
  }
}
