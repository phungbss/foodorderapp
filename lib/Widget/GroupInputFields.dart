import 'package:flutter/material.dart';

class GroupInputFields extends StatelessWidget {
  final List<Widget> items;

  GroupInputFields({this.items});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 25.0),
      padding: const EdgeInsets.only(left: 15.0, top: 0.0, bottom: 0.0),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(color: Colors.grey.withAlpha(100), width: 1.0),
            bottom: BorderSide(color: Colors.grey.withAlpha(100), width: 1.0),
          )
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items,
      ),
    );
  }
}

class GroupInputFieldsItem extends StatelessWidget {
  final String hint;
  final TextInputType textInputType;
  final Function onChanged;
  final bool isCreditCardField;
  final TextEditingController textEditingController;

  GroupInputFieldsItem({this.hint, this.textInputType, this.onChanged, this.isCreditCardField = false, this.textEditingController});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 3.0, bottom: 3.0),
      child: Row(
        children: <Widget>[
          isCreditCardField ? Padding(
            padding: const EdgeInsets.only(right: 6.0),
            child: Icon(Icons.credit_card, color: Colors.grey,),
          ) : Container(),
          Expanded(
            child: TextField(
              controller: textEditingController,
              onChanged: onChanged,
              keyboardType: textInputType,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: hint
              ),
            ),
          ),
        ],
      ),
    );
  }
}